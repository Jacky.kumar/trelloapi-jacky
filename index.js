const myKey = 'aac9edadcf3bb7a23c3813fbaa0a4519';
const myToken = '88060f24ced5e21bad034b1d1763915ab9189b12813166efc7b24b699d6cb396';
const myCardId = '5c9796ad1e4a5d458df2936a';
const myCheckItemsObj = `https://api.trello.com/1/cards/${myCardId}/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=${myKey}&token=${myToken}`;

const checklistDiv = document.querySelector('.listItem-container');

const getChecklistItems = (checkItemObj) => {
  checkItemObj[checkItemObj.length - 1].checkItems.forEach((checkItem) => {
    checklistDiv.id = checkItem.idChecklist;
    const checkStatus = checkItem.state === 'complete' ? 'checked' : '';

    const template = `<div id=${checkItem.id}><input type="checkbox" ${checkStatus}><label>${checkItem.name}</label><span class="span-cross">X</span></div>`;
    const itemsDiv = document.createElement('div');
    itemsDiv.innerHTML = template;
    document.querySelector('.listItem-container').appendChild(itemsDiv.firstChild);
  });
};
const fetchUrl = (url) => {
  fetch(url).then(data => data.json())
    .then((data) => {
      getChecklistItems(data);
    })
    .catch((err) => {
      alert('something went wrong');
      console.log(err);
    });
};
fetchUrl(myCheckItemsObj);

function addCheckListItem(checkItemName, checkListStatus, Key, Token, curItem) {
  fetch(`https://api.trello.com/1/checklists/5c98b5b03cefaa641f8d88a0/checkItems?name=${checkItemName}&pos=bottom&checked=${checkListStatus}&key=${Key}&token=${Token}`, {
    method: 'POST',
  })
    .then(res => res.json())
    .then((data) => {
      curItem.id = data.id;
    })
    .catch((err) => {
      alert('something went wrong');
      console.log(err);
    });
}

function addCheckItemUIandTrello() {
  const checkItemName = document.querySelector('.create-checklist input').value;
  document.querySelector('.create-checklist input').value = '';
  const template = `<div><input class="check-id" type="checkbox"><label>${checkItemName}</label><span class="span-cross">X</span></div>`;
  const itemsDiv = document.createElement('div');
  itemsDiv.innerHTML = template;
  const curItem = itemsDiv.firstChild;
  document.querySelector('.listItem-container').appendChild(itemsDiv.firstChild);
  const checkListStatus = document.querySelector('.check-id').checked;
  addCheckListItem(checkItemName, checkListStatus, myKey, myToken, curItem);
}

function createCheckListsItems() {
  const labelChecklist = document.querySelector('.create-checklist label');
  labelChecklist.addEventListener('click', addCheckItemUIandTrello);
}
createCheckListsItems();

function deleteChecklistItem(e) {
  if (e.target.matches('span')) {
    const checklistItem = e.target.parentNode;
    const checklist = e.target.parentNode.parentNode;
    fetch(`https://api.trello.com/1/checklists/${checklist.id}/checkItems/${checklistItem.id}?key=${myKey}&token=${myToken}`, {
      method: 'DELETE',
    })
      .then(res => res.json())
      .then(() => {
        e.target.parentNode.parentNode.removeChild(e.target.parentNode);
      })
      .catch((err) => {
        alert('something went wrong');
        console.log(err);
      });
  }
}

function updateCheckListItems(e) {
  if (e.target.matches('input')) {
    const checkStatus = e.target.checked === true ? 'complete' : 'incomplete';
    const checklistItem = e.target.parentNode;
    fetch(`https://api.trello.com/1/cards/${myCardId}/checkItem/${checklistItem.id}?state=${checkStatus}&key=${myKey}&token=${myToken}`, {
      method: 'PUT',
    })
      .then(res => res.json())
      .catch((err) => {
        e.target.checked = !e.target.checked;
        alert('something went wrong');
        console.log(err);
      });
  }
}
checklistDiv.addEventListener('click', deleteChecklistItem);
checklistDiv.addEventListener('change', updateCheckListItems);
